# otras funciones
# espacio preliminar para funciones que luego tendrán un lugar propio.

otras_funciones <- function(){
  
  # hallar correlacion entre:
  # clicks, costo, rebote
  # datos obtenidos en la siguiente funcion:
  # datos_diapositiva_8()
  correlacion <- function(){
    #install.packages(corrplot)
    library(corrplot)
    
    m <- cor(analytics_cost_bounce_8[,4:6])
    
    #dml() pertenece al paquete "rvg" para pasar graficos de r a ppt.
    m_plot <<- dml(code = corrplot(m))
   
  }
  correlacion()
  
  ####
  
  # nuevas funciones pueden crearse antes de cerrar 
  # otras_funciones()
  
  
  
  # aqui termina otras_funciones()
  # no colocar código debajo de esta línea.
}
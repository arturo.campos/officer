# https://gitlab.com/arturo.campos/officer.git



# cargar_ppt
# proposito: Crear ppt tema Attach mediante código.
# instalar package "officer"
# install.packages("officer")
cargar_ppt <- function(env=parent.frame()){
# cargar libreria officer
library("officer")
# magrittr para apoyo a officer
require("magrittr")

# cargar plantilla pptx attach
attach_ppt <<- read_pptx(path = "~/officer/themes/attach_patron_plantilla.pptx")

# verificar cantidad de diapositivas en el pptx importado.
# length(attach_ppt)
print("Para ver la plantilla creada llamar a la variable attach_ppt")
# listar los tipos de diapositivas en el documento y master disponibles
#layout_summary(attach_ppt)

}
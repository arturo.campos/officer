# generar cuadros editables en powerpoint
# ref: https://cran.r-project.org/web/packages/mschart/vignettes/introduction.html
# ref: https://cran.r-project.org/web/packages/mschart/vignettes/details.html
# ref: https://ardata-fr.github.io/mschart/
# ref: funciones dentro de mschart https://ardata-fr.github.io/mschart/reference/index.html
# ref: https://ardata-fr.github.io/mschart/articles/introduction.html

# ref: https://davidgohel.github.io/officer/articles/offcran/graphics.html#vector-graphics



agregar_cuadros <- function(){
  # install.packages("mschart")
  # install.packages("rvg")
  library("mschart")
  library("rvg")
      
      # diapositiva 8
      # grafico de ejemplo:
      # datos de ejemplo
      grafico_linea <- function(){
        
            linec <- ms_linechart(data = iris, x = "Sepal.Length",
                                  y = "Sepal.Width", group = "Species")
            linec <- chart_ax_y(linec, num_fmt = "0.00", rotation = -90)
            #modificar_diapositiva <- function(numero_de_diapositiva = 6){
            attach_ppt %>% 
              on_slide(index = 7) %>%
              #ph_with_chart funciona pero no sigue un estandar
              #ph_with_chart(chart = linec, type = "body" ) 
              ph_with(linec, location = ph_location(type = "body", right = 1))
              
              #}
              #modificar_diapositiva()
            
      }
      grafico_linea()
      
      # diapositiva 6
      # grafico de barra con data de Analytics
      # ref: https://ardata-fr.github.io/mschart/reference/mschart.html
      grafico_barra <- function(){
          
          # crear grafico de barra
          costo_por_campana <- ms_barchart(data = analytics_campaign_cost_7, x = "campaign", y = "adCost")
          
          # colocar en diapositiva 6
          attach_ppt %>%
            on_slide(index = 6) %>%
            ph_with(costo_por_campana, location = ph_location(top = 3, width = 10, height = 4))
          
        
      }
      grafico_barra()
      
      
      
      #diapositiva 8
      #grafico de correlacion 
      #no compatible con powerpoint
      
      grafico_correlacion <- function(){
        
        attach_ppt %>%
          on_slide(index = 8) %>%
          ph_with(value = m_plot, location = ph_location(left = 3, top = 3, width = 5, height = 4))
        
      }
      grafico_correlacion()
      
      
  # aqui acaba la funcion agregar_cuadros
}